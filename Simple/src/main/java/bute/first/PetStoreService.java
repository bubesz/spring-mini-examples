package bute.first;

/**
 * Created by Attila on 2015.02.22.
 */
public class PetStoreService {
    private ItemDao itemDao;
    private AccountDao accountDao;

    public PetStoreService(ItemDao itemDao) {
        this.itemDao = itemDao;
    }

    public static PetStoreService createInstance(ItemDao itemDao) {
        return new PetStoreService(itemDao);
    }

    public void sayHello() {
        System.out.println(itemDao.getName());
        System.out.println(accountDao.getName());
    }

    public AccountDao getAccountDao() {
        return accountDao;
    }

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }
}
