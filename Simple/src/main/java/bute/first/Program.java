package bute.first;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by Attila on a 2015.02.22.
 */
@Component
public class Program {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        PetStoreService storeService = context.getBean(PetStoreService.class);
        storeService.sayHello();
    }
}
