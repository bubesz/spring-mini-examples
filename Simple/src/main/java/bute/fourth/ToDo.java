package bute.fourth;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ToDo {
    public static void main(String[] args) {
        String[] contextPaths = new String[]{"swing-config.xml"};
        new ClassPathXmlApplicationContext(contextPaths);
    }
}