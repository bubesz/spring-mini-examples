package bute.second;

import bute.second.interfaces.Animal;
import bute.second.interfaces.Human;
import bute.second.interfaces.Traveller;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Attila on 2015.03.08.
 */
public class Hulk implements Animal, Human, Traveller {

    @Autowired
    private FoodService foodService;

    public void eat() {
        foodService.getFood();
        System.out.println("om-nom-nom");
    }

    @Override
    public void growl() {
        System.out.println("wraaar");
    }

    @Override
    public void think() {
        System.out.println("I think therefore I am");
    }

    @Override
    public void travel() {
        System.out.println("Let's head to a new place");
    }
}
