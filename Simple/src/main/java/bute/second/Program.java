package bute.second;

import bute.second.interfaces.Animal;
import bute.second.interfaces.Human;
import bute.second.interfaces.Traveller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by Attila on 2015.03.08.
 */
@Component
public class Program {
    @Autowired
    private Animal animalHulk;

    @Autowired
    private Human humanHulk;

    @Autowired
    private Traveller travellerHulk;

    @Autowired
    private Hulk hulk;

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        Program myProgram = context.getBean(Program.class);

        myProgram.hulk.eat();
        myProgram.animalHulk.growl();
        myProgram.travellerHulk.travel();
        myProgram.humanHulk.think();
    }
}
