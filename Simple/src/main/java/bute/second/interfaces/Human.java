package bute.second.interfaces;

/**
 * Created by Attila on 2015.03.08.
 */
public interface Human {
    public void think();
}
