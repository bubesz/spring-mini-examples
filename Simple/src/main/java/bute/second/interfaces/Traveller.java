package bute.second.interfaces;

/**
 * Created by Attila on 2015.03.08.
 */
public interface Traveller {
    public void travel();
}
