package bute.third;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by Attila on 2015.03.08.
 */
public class Person {

    @Min(0)
    private int age;

    @NotNull
    private String name;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
