package bute.third;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValues;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.ObjectError;

import java.util.HashMap;

/**
 * Created by Attila on 2015.03.08.
 */
public class Program {
    public static void main(String[] args) {
        Person person = new Person();
        HashMap<String, Object> properties = new HashMap<String, Object>();
        properties.put("name", "Attila");
        properties.put("age", -25);

        PropertyValues values = new MutablePropertyValues(properties);
        DataBinder dataBinder = new DataBinder(person);
        dataBinder.setValidator(new PersonValidator());

        dataBinder.bind(values);

        System.out.println("Name: " + person.getName());
        System.out.println("Age: " + person.getAge());

        dataBinder.validate();

        BindingResult result = dataBinder.getBindingResult();
        for (ObjectError error : result.getAllErrors()) {
            System.out.println(error);
        }
    }
}
